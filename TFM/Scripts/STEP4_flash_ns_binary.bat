echo OFF
echo "TFM_Appli NonSecure started"
setlocal
SET SCRIPT_DIR=%~dp0
call %SCRIPT_DIR%\..\..\Tools\Other\SetEnv.bat

set connect_no_reset="-c port=SWD mode=HotPlug"
set connect="-c port=SWD mode=HotPlug --hardRst"
:: part ot be updated according to flash_layout.h
set slot1=0x08038000
echo "Write TFM_Appli NonSecure""
%stm32programmercli% %connect% -d ..\STM32Cube_FW_L5_V1.3.0\Projects\NUCLEO-L552ZE-Q\Applications\TFM_for_WS\TFM_Appli\Binary\tfm_ns_sign.bin %slot1% -v 
echo "TFM_Appli NonSecure Written, press a key"
pause
