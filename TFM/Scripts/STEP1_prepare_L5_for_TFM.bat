echo off
setlocal
SET SCRIPT_DIR=%~dp0
call %SCRIPT_DIR%\..\..\Tools\Other\SetEnv.bat

echo "STEP1 prepare L5-Nucleo option byte for TFM script"
set connect_no_reset="-c port=SWD mode=HotPlug"
set do_mass_erase="-e all -rst"
set activate_TZ__ob="-ob  TZEN=1"
set default_ob1="-ob SRAM2_RST=0 SECBOOTADD0=0x180032 DBANK=1 SECWM1_PSTRT=0 SECWM1_PEND=111" 
set default_ob2="-ob SECWM2_PSTRT=127 SECWM2_PEND=0" 
%stm32programmercli% %connect_no_reset% %do_mass_erase%
%stm32programmercli% %connect_no_reset% %activate_TZ__ob%
%stm32programmercli% %connect_no_reset% %default_ob1%
%stm32programmercli% %connect_no_reset% %default_ob2%
echo "**************************************************************"
echo "Option setting"
echo "**************************************************************"
echo "TZ          : enabled "
echo "SRAM2_RST   : disabled "
echo "SECBOOTADD0 : 0x180032 -> 0x0C001900"
echo "DBANK       : enabled "
echo "SECWM1_PSTRT: 0x0      -> 0x08000000"
echo "SECWM1_PEND : 0x6F     -> 0x08037800"
echo "SECWM2_PSTRT: 0x7f     -> 0x0803f800"
echo "SECWM2_PEND : 0x00     -> 0x08000000"
echo "**************************************************************"
echo "Board is ready to receive the TFM binaries, press key"
pause
