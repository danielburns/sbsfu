echo off
setlocal
SET SCRIPT_DIR=%~dp0
call %SCRIPT_DIR%\..\..\Tools\Other\SetEnv.bat

echo "STEP5 Configure HDP
set connect_no_reset="-c port=SWD mode=HotPlug"
set hdp_config="-ob HDP1_PEND=28 HDP1EN=0x1" 
%stm32programmercli% %connect_no_reset% %hdp_config%

echo "**************************************************************"
echo "Option setting"
echo "**************************************************************"
echo "HDP1EN            : enabled "
echo "HDP1_PEND         : 0x1C  -> 0x0800E000 "
echo "**************************************************************"
echo "Board is ready to receive the TFM binaries, press key"
pause
