echo OFF
echo "TFM SBSFU started"
setlocal
SET SCRIPT_DIR=%~dp0
call %SCRIPT_DIR%\..\..\Tools\Other\SetEnv.bat

set connect="-c port=SWD mode=HotPlug --hardRst"
%stm32programmercli% %connect% -d ..\STM32Cube_FW_L5_V1.3.0\Projects\NUCLEO-L552ZE-Q\Applications\TFM_for_WS\TFM_SBSFU_Boot\STM32CubeIDE\Release\TFM_SBSFU_Boot.bin 0x0c001000 -v
echo "TFM SBSFU Done, press a key"
pause