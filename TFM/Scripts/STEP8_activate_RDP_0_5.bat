echo off
setlocal
SET SCRIPT_DIR=%~dp0
call %SCRIPT_DIR%\..\..\Tools\Other\SetEnv.bat
echo "STEP6 Activate RDP 0.5"
set connect_no_reset="-c port=SWD mode=HotPlug"
set activate_rdp_0_5="-ob RDP=0x55"
%stm32programmercli% %connect_no_reset% %activate_rdp_0_5%
echo "**************************************************************"
echo "RDP 0.5 activated !"
pause
