echo OFF
echo "TFM_Appli NonSecure started"
setlocal
SET SCRIPT_DIR=%~dp0
call %SCRIPT_DIR%\..\..\Tools\Other\SetEnv.bat

set connect_no_reset="-c port=SWD mode=HotPlug"
set connect="-c port=SWD mode=HotPlug --hardRst"
set do_mass_erase="-e all -rst"
%stm32programmercli% %connect_no_reset% %do_mass_erase%

:: part ot be updated according to flash_layout.h
set slot1=0x08038000
echo "Write TFM_Appli NonSecure""
%stm32programmercli% %connect% -d ..\STM32Cube_FW_L5_V1.3.0\Projects\NUCLEO-L552ZE-Q\Applications\TFM_for_WS\Archive\tfm_ns_sign.bin %slot1% -v 
echo "TFM_Appli NonSecure Written, press a key to flash the TFM_Appli Secure"
pause
set slot0=0x0c014000
echo "Write TFM_Appli Secure""
%stm32programmercli% %connect% -d ..\STM32Cube_FW_L5_V1.3.0\Projects\NUCLEO-L552ZE-Q\Applications\TFM_for_WS\Archive\tfm_s_sign.bin %slot0% -v
echo "TFM_Appli Secure Written, press a key to flash the "
pause
set connect="-c port=SWD mode=HotPlug --hardRst"
%stm32programmercli% %connect% -d ..\STM32Cube_FW_L5_V1.3.0\Projects\NUCLEO-L552ZE-Q\Applications\TFM_for_WS\Archive\TFM_SBSFU_Boot.bin 0x0c001000 -v
echo "TFM SBSFU Done, press a key"
pause