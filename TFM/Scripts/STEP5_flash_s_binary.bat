echo OFF
echo "TFM_Appli Secure started"
setlocal
SET SCRIPT_DIR=%~dp0
call %SCRIPT_DIR%\..\..\Tools\Other\SetEnv.bat

set connect_no_reset="-c port=SWD mode=HotPlug"
set connect="-c port=SWD mode=HotPlug --hardRst"
:: part ot be updated according to flash_layout.h
set slot0=0x0c014000
echo "Write TFM_Appli Secure""
%stm32programmercli% %connect% -d ..\STM32Cube_FW_L5_V1.3.0\Projects\NUCLEO-L552ZE-Q\Applications\TFM_for_WS\TFM_Appli\Binary\tfm_s_sign.bin %slot0% -v
echo "TFM_Appli Secure Written, press a key"
pause
