echo off
setlocal
SET SCRIPT_DIR=%~dp0
call %SCRIPT_DIR%\..\..\Tools\Other\SetEnv.bat

echo "STEP11 Do a transition RDP 0.5 the RDP 0 with Trustzone removed !"
echo " WARNING : IF THE BLUE/GREEN LED ARE NOT BLINKING, INTERRUPT THIS SCRIPT (CTRL+C)"
Pause
echo " ARE YOU SURE THE BLUE/GREEN LED BLINKING ? IF NOT INTERRUPT THIS SCRIPT (CTRL+C)"
Pause
@echo off

@echo on
%stm32programmercli% -c port=swd mode=HOTPLUG -ob RDP=0x55
@echo Please make sure leds are blinking before continuing ( if not press reset button your board )
pause
%stm32programmercli% -c port=swd mode=HOTPLUG -ob TZEN=0 RDP=0xAA
%stm32programmercli% -c port=swd mode=HOTPLUG -ob displ
@echo off
pause
ENDLOCAL
pause
