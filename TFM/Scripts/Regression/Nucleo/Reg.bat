@echo off

setlocal
SET SCRIPT_DIR=%~dp0
call %SCRIPT_DIR%\..\..\..\..\Tools\Other\SetEnv.bat

@echo on
%prg% -c port=swd mode=HOTPLUG -ob RDP=0x55
@echo Please restart the board and make sure leds are blinking before continuing
pause
%prg% -c port=swd mode=HOTPLUG -ob TZEN=0 RDP=0xAA
%prg% -c port=swd mode=HOTPLUG -ob displ
@echo off
pause
ENDLOCAL