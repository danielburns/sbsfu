echo off
setlocal
SET SCRIPT_DIR=%~dp0
call %SCRIPT_DIR%\..\..\Tools\Other\SetEnv.bat

@echo on
%stm32programmercli% -c port=swd mode=HOTPLUG -ob TZEN=1
%stm32programmercli% -c port=swd mode=HOTPLUG -ob SECBOOTADD0=0x180000 
%stm32programmercli% -c port=swd mode=HOTPLUG -ob SECWM1_PSTRT=0 SECWM1_PEND=0x7F 
%stm32programmercli% -c port=swd mode=HOTPLUG -ob SECWM2_PSTRT=0x7F SECWM2_PEND=0x0 
%stm32programmercli% -c port=swd mode=HOTPLUG -e all -rst
%stm32programmercli% -c port=swd mode=HOTPLUG -d Regression\Nucleo\Project_ns.hex -v -rst
%stm32programmercli% -c port=swd mode=HOTPLUG -d Regression\Nucleo\Project_s.hex -v -rst

@echo off

ENDLOCAL
pause
