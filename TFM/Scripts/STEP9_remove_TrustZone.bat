echo off
setlocal
SET SCRIPT_DIR=%~dp0
call %SCRIPT_DIR%\..\..\Tools\Other\SetEnv.bat

echo "STEP7 remove TrustZone thansk RDP regression and also WRP"
set connect_no_reset="-c port=SWD mode=HotPlug"
set deactivate_TZ_ob="-ob TZEN=0 RDP=0xAA"
set remove_wrp="-ob WRP1A_PSTRT=0x7f WRP1A_PEND=0" 
%stm32programmercli% %connect_no_reset% %deactivate_TZ_ob%
%stm32programmercli% %connect_no_reset% %remove_wrp%
echo "remove TrustZone thanks RDP regression and also WRP done, press key"
pause
