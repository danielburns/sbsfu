echo OFF
echo "TFM_Appli NonSecure started"
setlocal
SET SCRIPT_DIR=%~dp0
call %SCRIPT_DIR%\..\..\Tools\Other\SetEnv.bat

set connect_no_reset="-c port=SWD mode=HotPlug"
set connect="-c port=SWD mode=HotPlug --hardRst"
set do_mass_erase="-e all -rst"
%stm32programmercli% %connect_no_reset%  %do_mass_erase% 
echo "Mass erase done, press a key"
pause
