/**
  ******************************************************************************
  * @file    sfu_low_level_flash_ext.c
  * @author  MCD Application Team
  * @brief   SFU Flash Low Level Interface module
  *          This file provides set of firmware functions to manage SFU external
  *          flash low level interface.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2017 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file in
  * the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "sfu_low_level_flash_ext.h"
#include "sfu_low_level_security.h"
#include "stm32f7xx_hal_qspi.h"
#include "sfu_trace.h"
#include "string.h"

/* Private defines -----------------------------------------------------------*/
/* N25Q128A13EF840E Micron memory */
/* Size of the flash */
#define QSPI_FLASH_SIZE                      23   // Number of address bits required to address 16MB
#define QSPI_PAGE_SIZE                       256


/* Identification Operations */
#define READ_ID_CMD                          0x9E
#define READ_ID_CMD2                         0x9F
#define QUAD_INOUT_FAST_READ_CMD             0xEB
#define MULTIPLE_IO_READ_ID_CMD              0xAF
#define READ_SERIAL_FLASH_DISCO_PARAM_CMD    0x5A
#define N25Q128A_DUMMY_CYCLES_READ_QUAD      10

/* Functions Definition ------------------------------------------------------*/


/* No external flash available on this product
   ==> return SFU_ERROR except for SFU_LL_FLASH_EXT_Init and SFU_LL_FLASH_EXT_Config_Exe which are called
       systematically during startup phase */

SFU_ErrorStatus QSPI_EnableMemoryMappedMode(QSPI_HandleTypeDef* QSPIHandle)
{
  QSPI_CommandTypeDef      sCommand;
  QSPI_MemoryMappedTypeDef sMemMappedCfg;

  /* Configure the command for the read instruction */
  sCommand.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
  sCommand.Instruction       = QUAD_INOUT_FAST_READ_CMD;
  sCommand.AddressMode       = QSPI_ADDRESS_4_LINES;
  sCommand.AddressSize       = QSPI_ADDRESS_24_BITS;
  sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
  sCommand.DataMode          = QSPI_DATA_4_LINES;
  sCommand.DummyCycles       = N25Q128A_DUMMY_CYCLES_READ_QUAD;
  sCommand.DdrMode           = QSPI_DDR_MODE_DISABLE;
  sCommand.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
  sCommand.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;

  /* Configure the memory mapped mode */
  sMemMappedCfg.TimeOutActivation = QSPI_TIMEOUT_COUNTER_ENABLE;
  sMemMappedCfg.TimeOutPeriod     = 4; /* 50 ns (4 periods of a 80 MHz clock) */

  if (HAL_QSPI_MemoryMapped(QSPIHandle, &sCommand, &sMemMappedCfg) != HAL_OK)
  {
    return SFU_ERROR;
  }

  return SFU_SUCCESS;
}

SFU_ErrorStatus SFU_LL_FLASH_EXT_Init(void)
{

	TRACE("\r\nSFU_LL_FLASH_EXT_Init\r\n");

	QSPI_HandleTypeDef QSPIHandle;
	QSPI_CommandTypeDef sCommand;

	QSPIHandle.Instance = QUADSPI;
	HAL_QSPI_DeInit(&QSPIHandle);

	QSPIHandle.Init.ClockPrescaler     = 1; /* ClockPrescaler set to 1, so QSPI clock = 80MHz / (1+1) = 40MHz */
	QSPIHandle.Init.FifoThreshold      = 4;
	QSPIHandle.Init.SampleShifting     = QSPI_SAMPLE_SHIFTING_NONE;
	QSPIHandle.Init.FlashSize          = QSPI_FLASH_SIZE;
	QSPIHandle.Init.ChipSelectHighTime = QSPI_CS_HIGH_TIME_1_CYCLE;
	QSPIHandle.Init.ClockMode          = QSPI_CLOCK_MODE_0;

	if (HAL_QSPI_Init(&QSPIHandle) != HAL_OK)
	{
	  TRACE("HAL_QSPI_Init() failed!\n");
	  while(1);
	}

	uint32_t id = 0;

	/* Read device ID register --------------------------- */
	sCommand.InstructionMode   = QSPI_INSTRUCTION_1_LINE;
	sCommand.Instruction       = READ_ID_CMD2;
	sCommand.AddressMode       = QSPI_ADDRESS_NONE;
	sCommand.AlternateByteMode = QSPI_ALTERNATE_BYTES_NONE;
	sCommand.DataMode          = QSPI_DATA_1_LINE;
	sCommand.DummyCycles       = 0;
	sCommand.DdrMode           = QSPI_DDR_MODE_DISABLE;
	sCommand.DdrHoldHalfCycle  = QSPI_DDR_HHC_ANALOG_DELAY;
	sCommand.SIOOMode          = QSPI_SIOO_INST_EVERY_CMD;
	sCommand.NbData            = 4;

	if (HAL_QSPI_Command(&QSPIHandle, &sCommand, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
	{
	    TRACE("HAL_QSPI_Command() failed!\n");
	    while(1);
	}

	if (HAL_QSPI_Receive(&QSPIHandle, (uint8_t*)&id, HAL_QPSI_TIMEOUT_DEFAULT_VALUE) != HAL_OK)
	{
		TRACE("HAL_QSPI_Receive() failed!\n");
	    while(1);
	}

	if(id == 0x1018BA20)
		TRACE("Micron QSPI Flash Detected\n");
	else
		TRACE("QSPI Flash ID: 0x%X\r\n", id);

	// switch over to quad mode and memory mapped mode
	TRACE("Configuring memory mapped mode\n");

	if (QSPI_EnableMemoryMappedMode(&QSPIHandle) != SFU_SUCCESS)
	{
		TRACE("BSP_QSPI_EnableMemoryMappedMode() failed!\n");
		while(1);
	}

	TRACE("Configured qspi flash for memory mapped mode: [0x90000000] = 0x%X", *((unsigned int*)0x90000000));



  return SFU_SUCCESS;
}

SFU_ErrorStatus SFU_LL_FLASH_EXT_Erase_Size(SFU_FLASH_StatusTypeDef *pFlashStatus, uint8_t *pStart, uint32_t Length)
{
  UNUSED(pFlashStatus);
  UNUSED(pStart);
  UNUSED(Length);
  return SFU_ERROR;
}

SFU_ErrorStatus SFU_LL_FLASH_EXT_Write(SFU_FLASH_StatusTypeDef *pFlashStatus, uint8_t  *pDestination,
                                       const uint8_t *pSource, uint32_t Length)
{
  UNUSED(pFlashStatus);
  UNUSED(pDestination);
  UNUSED(pSource);
  UNUSED(Length);
  TRACE("TODO SFU_LL_FLASH_EXT_Write\n");
  return SFU_ERROR;
}

SFU_ErrorStatus SFU_LL_FLASH_EXT_Read(uint8_t *pDestination, const uint8_t *pSource, uint32_t Length)
{
  // TODO validate parameters

  // qspi is memory mapped, so just read from it

  (void) memcpy(pDestination, pSource, Length);

  return SFU_SUCCESS;
}

SFU_ErrorStatus SFU_LL_FLASH_EXT_Compare(const uint8_t *pFlash, const uint32_t Pattern1, const uint32_t Pattern2, uint32_t Length)
{

  TRACE("SFU_LL_FLASH_EXT_Compare\n");

  uint32_t flash = (uint32_t) pFlash;
  uint32_t i;

  /* Comparison executed by SBSFU ==> flash area could not be located inside secured environment */
  for ( i = 0U; i < Length; i += 4U)
  {
    if ((*(uint32_t *)(flash + i) != Pattern1) &&  (*(uint32_t *)(flash + i) != Pattern2))
    {
      return SFU_ERROR;
    }
  }
  return SFU_SUCCESS;

}
SFU_ErrorStatus SFU_LL_FLASH_EXT_Config_Exe(uint32_t SlotNumber)
{
  UNUSED(SlotNumber);
  TRACE("TODO SFU_LL_FLASH_EXT_Config_Exe\n");
  return SFU_SUCCESS;
}
