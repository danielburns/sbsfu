@echo off

setlocal
SET SCRIPT_DIR=%~dp0
call %SCRIPT_DIR%\..\..\Tools\Other\SetEnv.bat

@echo on
%stm32programmercli%  -c port=SWD mode=UR -e all --skipErase -d C:\Users\daniel.burns\STM32CubeIDE\workspace_1.7.0\STM32F746G-DISCO-example1\Debug\STM32F746G-DISCO-example1.bin 0x08000000

pause
endlocal
