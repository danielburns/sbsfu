@echo off

setlocal
SET SCRIPT_DIR=%~dp0
call %SCRIPT_DIR%\..\..\Tools\Other\SetEnv.bat

@echo on
goto F7
:H7
%stm32programmercli% -c ^
 port=SWD mode=UR ^
 -ob RDP=0xBB ^
 -ob RDP=0xAA ^
  RSS1=0x00 ^
  BOR_LEV=0x00 ^
  IWDG1_SW=0x01 ^
  NRST_STOP_D1=0x01 ^
  NRST_STBY_D1=0x01 ^
  FZ_IWDG_STOP=0x01 ^
  FZ_IWDG_SDBY=0x01 ^
  SECURITY=0x00 ^
  BCM7=0x01 ^
  NRST_STOP_D2=0x01 ^
  NRST_STBY_D2=0x01 ^
  SWAP_BANK=0x00 ^
  IO_HSLV=0x00 ^
  BOOT_CM7_ADD0=0x0800 ^
  BOOT_CM7_ADD1=0x1FF0 ^
  PROT_AREA_START1=0xFF ^
  PROT_AREA_END1=0x00 ^
  DMEP1=0x00 ^
  PROT_AREA_START2=0xFF ^
  PROT_AREA_END2=0x00 ^
  DMEP2=0x00 ^
  SEC_AREA_START1=0xFF ^
  SEC_AREA_END1=0x00 ^
  DMES1=0x00 ^
  SEC_AREA_START2=0xFF ^
  SEC_AREA_END2=0x00 ^
  DMES2=0x00 ^
  ST_RAM_SIZE=0x00 ^
  nWRP0=0x01 ^
  nWRP1=0x01 ^
  nWRP2=0x01 ^
  nWRP3=0x01 ^
  nWRP4=0x01 ^
  nWRP5=0x01 ^
  nWRP6=0x01 ^
  nWRP7=0x01 ^
  nWRP8=0x01 ^
  nWRP9=0x01 ^
  nWRP10=0x01 ^
  nWRP11=0x01 ^
  nWRP12=0x01 ^
  nWRP13=0x01 ^
  nWRP14=0x01 ^
  nWRP15=0x01 ^
 -ob displ
:F7
%stm32programmercli% -c ^
 port=SWD mode=UR ^
 -ob RDP=0xBB ^
 -ob RDP=0xAA ^
  BOR_LEV=0x00 ^
  IWDG_STOP=0x1 ^
  IWDG_STDBY=0x1 ^
  WWDG_SW=0x1 ^
  IWDG_SW=0x1 ^
  nRST_STOP=0x1 ^
  nRST_STDBY=0x1 ^
  nWRP0=0x01 ^
  nWRP1=0x01 ^
  nWRP2=0x01 ^
  nWRP3=0x01 ^
  nWRP4=0x01 ^
  nWRP5=0x01 ^
  nWRP6=0x01 ^
  nWRP7=0x01 ^
 -ob displ
pause
endlocal
