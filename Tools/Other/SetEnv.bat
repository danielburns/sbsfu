set ProgX86="C:\Program Files (x86)\STMicroelectronics\STM32Cube\STM32CubeProgrammer\bin\STM32_Programmer_CLI.exe"
set Prog64="C:\STMicroelectronics\STM32Cube\STM32CubeProgrammer\bin\STM32_Programmer_CLI.exe"

if exist  %Prog64% (
set stm32programmercli=%Prog64%
) else (
	if exist %ProgX86% (
		set stm32programmercli=%ProgX86%
	) else 	(
		echo ****************ERROR*********************
		echo STM32CubeProgrammer not found
		echo Please update STM32CubeProgrammer path in C:\STM32SecuWS\Tools\Other\SetEnv.bat
		echo ******************************************
		pause
	)
)

