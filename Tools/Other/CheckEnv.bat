echo off
setlocal
SET SCRIPT_DIR=%~dp0
call %SCRIPT_DIR%\SetEnv.bat
echo ******************************************************************
echo **************** Check CubeProgramer version *********************
echo ******************************************************************
echo It should be STM32CubeProgrammer version: 2.5.0*******************
%stm32programmercli% -version 
echo ******************************************************************
echo If the version displayed is not correct please install 
echo      STM32CubeProgrammer version: 2.5.0
echo Or update STM32CubeProgrammer path in the following script to point
echo on the version 2.5 :
echo      C:\STM32SecuWS\Tools\Other\SetEnv.bat
echo ******************************************************************
pause
echo ******************************************************************
echo **************** Check SBSFU installation     ********************
echo ******************************************************************
if exist  %SCRIPT_DIR%..\..\L4\STM32CubeExpansion_SBSFU_V2.4.0\Projects (
echo SBSFU installation is OK !
echo ******************************************************************
) else (
echo FAILED : SBSFU not correctly install 
echo This path doesn't exist :
echo %SCRIPT_DIR%..\..\L4\STM32CubeExpansion_SBSFU_V2.4.0
echo Please unzip STM32CubeExpansion_SBSFU_V2.4.0.zip at this location 
echo %SCRIPT_DIR%..\..\L4\
echo ******************************************************************
)
pause